<?php
/**
 *
* @package Zmsmessaging
*
*/
namespace BO\Zmsmessaging;

use BO\Zmsentities\Mail;
use BO\Zmsentities\Mimepart;
use BO\Mellon\Validator;
use BO\Zmsentities\Schema\Entity;

class BaseController
{
    protected $verbose = false;
    protected static $logList = [];
    protected $workstation = null;
    protected $startTime;
    protected $maxRunTime = 50;
    protected $idEndings; // process ids with these endings only (id modulo 10)

    public function __construct($verbose = false, array $idEndings = [0,1,2,3,4,5,6,7,8,9], $maxRunTime = 50)
    {
        $this->verbose = $verbose;
        $this->startTime = microtime(true);
        $this->maxRunTime = $maxRunTime;
        $this->idEndings = $idEndings;
    }

    public function log($message)
    {
        $time = $this->getSpendTime();
        $memory = memory_get_usage()/(1024*1024);
        $text = sprintf("[Init Messaging log %07.3fs %07.1fmb] %s", "$time", $memory, $message);
        static::$logList[] = $text;
        if ($this->verbose) {
            error_log('verbose is: '. $this->verbose);
            error_log($text);
        }
        return $this;
    }

    public static function getLogList()
    {
        return static::$logList;
    }

    public static function clearLogList()
    {
        static::$logList = [];
    }

    protected function getSpendTime()
    {
        $time = round(microtime(true) - $this->startTime, 3);
        return $time;
    }

    protected function shouldIgnoreItem(Entity $item): bool
    {
        return !in_array((int) $item->getId() % 10, $this->idEndings);
    }

    protected function sendMailer(\BO\Zmsentities\Schema\Entity $entity, $mailer = null, $action = false)
    {
        if ($action === false) {
            return $mailer;
        }
        try {
            $success = $mailer->Send();
        } catch (\Exception $exception) {
            $this->log("Exception: SendingFailed  - ". $exception->getMessage() .' | '. \App::$now->format('c'));
            $this->deleteEntityFromQueue($entity);
            throw new Exception\SendingFailed();
        }
        // @codeCoverageIgnoreStart
        if ($success === false) {
            $this->removeEntityOlderThanOneHour($entity);
            return $mailer;
        }
        $this->log("Send Mailer: sending succeeded - ". \App::$now->format('c'));
        $log = new Mimepart(['mime' => 'text/plain']);
        $log->content = ($entity instanceof Mail) ? $entity->subject : $entity->message;
        $this->log("Send Mailer: log readPostResult start - ". \App::$now->format('c'));
        \App::$http->readPostResult('/log/process/'. $entity->process['id'] .'/', $log);
        $this->log("Send Mailer: log readPostResult finished - ". \App::$now->format('c'));
        return $mailer;
        // @codeCoverageIgnoreEnd
    }

    protected function removeEntityOlderThanOneHour($entity): void
    {
        if (3600 < \App::$now->getTimestamp() - $entity->createTimestamp) {
            $this->log("Delete Entity: removeEntityOlderThanOneHour start - ". \App::$now->format('c'));
            $this->deleteEntityFromQueue($entity);
            $this->log("Delete Entity: removeEntityOlderThanOneHour finished - ". \App::$now->format('c'));
            $log = new Mimepart(['mime' => 'text/plain']);
            $log->content = 'Zmsmessaging Failure: Queue entry older than 1 hour has been removed';
            $this->log("Delete Entity: log readPostResult start - ". \App::$now->format('c'));
            \App::$http->readPostResult('/log/process/'. $entity->process['id'] .'/', $log, ['error' => 1]);
            \App::$log->warning($log->content);
            $this->log("Delete Entity: log readPostResult finished - ". \App::$now->format('c'));
        }
    }

    public function deleteEntityFromQueue($entity)
    {
        $type = ($entity instanceof \BO\Zmsentities\Mail) ? 'mails' : 'notification';
        try {
            $this->log("Delete Entity: readDeleteResult start - ". \App::$now->format('c'));
            $entity = \App::$http->readDeleteResult('/'. $type .'/'. $entity->id .'/')->getEntity();
            $this->log("Delete Entity: readDeleteResult finished - ". \App::$now->format('c'));
        } catch (\BO\Zmsclient\Exception $exception) {
            //ignore excpeption if mail not found
            if ($exception->template != 'BO\Zmsapi\Exception\Mail\MailNotFound') {
                throw $exception;
            }
        }
        return (bool)$entity;
    }

    public function testEntity($entity)
    {
        if (!isset($entity['department'])) {
            throw new \Exception("Could not resolve department for message ".$entity['id']);
        }
        if (!isset($entity['department']['email'])) {
            throw new \Exception(
                "No mail address for department "
                .$entity['department']['name']
                ." (departmentID="
                .$entity['department']['id']
                ." Vorgang="
                .$entity['process']['id']
                .") "
                .$entity['id']
            );
        }
        if (! $entity->hasContent()) {
            throw new \BO\Zmsmessaging\Exception\MailWithoutContent();
        }

        if ($entity instanceof Mail && ! Validator::value($entity->getRecipient())->isMail()->hasDNS()->getValue()) {
            throw new \BO\Zmsmessaging\Exception\InvalidMailAddress();
        }
    }
}
